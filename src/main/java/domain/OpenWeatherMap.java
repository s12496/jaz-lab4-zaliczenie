package domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OpenWeatherMap {

	private String miasto;
	private int poziomZachmurzenia;
	private double temperatura;
	private double cisnienie;
	private double predkoscWiatru;

	private String prognozaOgolna;
	private String prognoza;

	
	public String getMiasto() {
		return miasto;
	}
	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}
	public int getPoziomZachmurzenia() {
		return poziomZachmurzenia;
	}
	public void setPoziomZachmurzenia(int poziomZachmurzenia) {
		this.poziomZachmurzenia = poziomZachmurzenia;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	public double getCisnienie() {
		return cisnienie;
	}
	public void setCisnienie(double cisnienie) {
		this.cisnienie = cisnienie;
	}
	public double getPredkoscWiatru() {
		return predkoscWiatru;
	}
	public void setPredkoscWiatru(double predkoscWiatru) {
		this.predkoscWiatru = predkoscWiatru;
	}
	public String getPrognozaOgolna() {
		return prognozaOgolna;
	}
	public void setPrognozaOgolna(String prognozaOgolna) {
		this.prognozaOgolna = prognozaOgolna;
	}
	public String getPrognoza() {
		return prognoza;
	}
	public void setPrognoza(String prognoza) {
		this.prognoza = prognoza;
	} 

}

package services;

import java.io.IOException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import domain.OpenWeatherMap;


public class OpenWeatherMapApiService {


    public static OpenWeatherMap main(String prognozaBiezaca) {
    
    	final String URL = "http://api.openweathermap.org/data/2.5/weather?q=" 
    	+ prognozaBiezaca 
    	+ ",pl&appid=2a5d7cf5249be6c506ae790c875d0e53";
     	
    	OpenWeatherMap prognoza = new OpenWeatherMap();
        String result = "";
         
        try {
            URL weather = new URL(URL);
 
            HttpURLConnection httpURLConnection = (HttpURLConnection) weather.openConnection();
 
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
 
                InputStreamReader reader =
                    new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader buffered =
                    new BufferedReader(reader, 8192);
                String line = null;
                while((line = buffered.readLine()) != null){
                    result = result + line;
                }
                  
                buffered.close();
                 	      
    	        JSONObject jsonObject = new JSONObject(result);
    	 
    	       JSONArray weatherArray = jsonObject.getJSONArray("weather");
    	        if(weatherArray.length() > 0){
    	            JSONObject mainWeather = weatherArray.getJSONObject(0);
    	            
    	            prognoza.setPrognozaOgolna(mainWeather.getString("main"));         
    	            prognoza.setPrognoza(mainWeather.getString("description")); 
    	          
    	        }

    	        JSONObject main = jsonObject.getJSONObject("main");

    	        	prognoza.setTemperatura(main.getDouble("temp")-273.15); 
    	        	prognoza.setCisnienie(main.getDouble("pressure"));

    	        JSONObject wind = jsonObject.getJSONObject("wind");  
    	        	prognoza.setPredkoscWiatru(wind.getDouble("speed"));

    	        JSONObject clouds = jsonObject.getJSONObject("clouds");
    	        	prognoza.setPoziomZachmurzenia(clouds.getInt("all"));     	      

            } else { 
            	System.out.println("Błąd komunikacji...");
            }
 
        } catch (MalformedURLException x) {
            Logger.getLogger(OpenWeatherMapApiService.class.getName()).log(Level.SEVERE, null, x);
        } catch (IOException x) {
            Logger.getLogger(OpenWeatherMapApiService.class.getName()).log(Level.SEVERE, null, x);
        } catch (JSONException x) {
            Logger.getLogger(OpenWeatherMapApiService.class.getName()).log(Level.SEVERE, null, x);
        }
        
        return prognoza;
    }
	
}
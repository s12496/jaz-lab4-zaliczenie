<%@page import="domain.OpenWeatherMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Serwis pogodowy</title>
</head>
<body>

<jsp:useBean id="api" class="services.OpenWeatherMapApiService" scope="application"/>
<jsp:useBean id="prognoza" class="domain.OpenWeatherMap" scope="session"/>
<jsp:setProperty name="prognoza" property="miasto" param="miasto"/>

<h2>Oto prognoza pogody dla: <jsp:getProperty name="prognoza" property="miasto"/></h2>
 
Poziom zachmurzenia: <% out.println(api.main(prognoza.getMiasto()).getPoziomZachmurzenia()); %>%</br>
Temperatura: <% out.println(Math.round(api.main(prognoza.getMiasto()).getTemperatura() * 100d) / 100d); %>°C</br>
Ciśnienie: <% out.println(api.main(prognoza.getMiasto()).getCisnienie());%>hPa</br>
Prędkość wiatru: <% out.println(api.main(prognoza.getMiasto()).getPredkoscWiatru()); %> m/s

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Serwis pogodowy</title>
</head>
<body>

<jsp:useBean id="api" class="services.OpenWeatherMapApiService" scope="application"/>
<jsp:useBean id="prognoza" class="domain.OpenWeatherMap" scope="session"/>

<form action="prognoza.jsp">

<h3>Wybierz miasto:</h3>
<label><input type="radio" value="Warszawa" name="miasto" id="miasto"/>Warszawa</label><br/>
<label><input type="radio" value="Gdansk" name="miasto" id="miasto" checked/>Gdańsk</label><br/>
<label><input type="radio" value="Krakow" name="miasto" id="miasto"/>Kraków</label><br/>
<label><input type="radio" value="Wroclaw" name="miasto" id="miasto"/>Wrocław</label><br/>
<label><input type="radio" value="Poznan" name="miasto" id="miasto"/>Poznań</label><br/>
<label><input type="radio" value="Lodz" name="miasto" id="miasto"/>Łódź</label><br/>
<label><input type="radio" value="Katowice" name="miasto" id="miasto"/>Katowice</label><br/><br/>

<input type = "submit" value="Dalej"/>
</form>
</body>
</html>